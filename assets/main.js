$.fn.digits = function () {
    return this.each(() => {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
    })
};

let totalAmount = 0;

$(() => {
    $('#payInvoice').click((e) => {
        $.ajax({
            type: 'POST',
            url: '/total',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                amount: totalAmount
            }),
            success({amount, info, terminalId, id}) {
                    e.preventDefault();
                Pocket.payButton({
                            amount: amount,
                            info: info,
                            terminalId: terminalId,
                            id: id
                        },
                        (params) => {
                            if (params.error) {
                                console.log(params.errorMessage);
                            } else {
                                alert('success');
                            }
                        })
            },
            error: (jqXHR, textStatus, err) => {
                console.error(textStatus + err);
            },
        })
    });

    $('.add').click((e) => {
        const price = parseInt($(e.target).siblings().data('price'));

        if ($(e.target).text() === 'Нэмэх') {
            totalAmount += price;
            $(e.target).removeClass('add-css').addClass('delete-css');
            $(e.target).text('Устгах');
        } else {
            totalAmount -= price;
            $(e.target).removeClass('delete-css').addClass('add-css');
            $(e.target).text('Нэмэх');
        }
        $('#total-sum').text(`${totalAmount} ₮`).digits();
    });
});
