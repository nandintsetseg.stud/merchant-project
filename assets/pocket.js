window.Pocket = {
    eventListeners: {
        onPayInvoiceComplete: () => {
            throw new NotImplementedException('This method is not implemented.');
        },
    },

    payInvoice(data, callback) {
        this.eventListeners.onPayInvoiceComplete = callback;
        window.PocketMobile.postMessage(JSON.stringify({action: 'payInvoice', data}));
    },

    payButton(data, callback) {
        var element = document.createElement("button");
        element.innerText = "Төлөх";
        element.classList.add("button-pocket");
        document.getElementById("total").appendChild(element);
        element.addEventListener("click", function(){
            Pocket.payInvoice(data,callback);
        })
    }
};