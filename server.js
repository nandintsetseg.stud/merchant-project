const fs = require('fs');
const qs = require('qs');
const axios = require("axios");
const express = require('express');
const bodyParser = require('body-parser');
const settings = require('./settings.js');
const mustacheExpress = require('mustache-express');

const app = express();
const PORT = settings.data.port;

app.use('/assets', express.static(__dirname + '/assets'));
app.use(bodyParser.json());

app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

let accessToken;
let refreshToken;
let clientId = settings.data.client_id;
let clientSecret = settings.data.client_secret;
let consumerAccessToken;
let consumerRefreshToken;

const allItems = JSON.parse(fs.readFileSync('data.json', 'utf8'));

const getToken = async (tokenRequest, path = '') => axios({
    method: 'POST',
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    data: qs.stringify(tokenRequest),
    url: `http://192.168.3.90:8080/auth/realms/invescore/protocol/openid-connect/token${path}`,
});

const authMiddleware = async (req, res, next) => {
    consumerAccessToken = req.query.access_token;
    consumerRefreshToken = req.query.refresh_token;
    try {
        const { data } = await getToken({
            grant_type: "client_credentials",
            client_id: clientId,
            client_secret: clientSecret
        });
        accessToken = data.access_token;
        refreshToken = data.refresh_token;
    } catch (e) {
        res.send(e);
    }
    next();
};

const checkMiddleware = async (req, res, next) => {
    try {
        const { data } = await getToken({
                client_id: clientId,
                client_secret: clientSecret,
                token: consumerAccessToken
            }, '/introspect'
        );
        if (!data.active) {
            try {
                const { data } = await getToken({
                    grant_type: "refresh_token",
                    client_id: clientId,
                    client_secret: clientSecret,
                    refresh_token: consumerRefreshToken
                });
                consumerAccessToken = data.access_token;
                consumerRefreshToken = data.refresh_token;
            } catch (e) {
                res.send(e);
            }
        }
    } catch (e) {
        res.send(e);
    }

    try {
        const { data } = await getToken({
                client_id: clientId,
                client_secret: clientSecret,
                token: accessToken
            }, '/introspect'
        );
        if (!data.active) {
            try {
                getToken({
                        grant_type: "refresh_token",
                        client_id: clientId,
                        client_secret: clientSecret,
                        refresh_token: refreshToken
                    }
                );
                accessToken = data.access_token;
            } catch (e) {
                res.send(e);
            }
        }
    } catch (e) {
        res.send(e);
    }
    next();
};

app.get('/', authMiddleware, (req, res) => {
    res.render('index', {
        items: allItems,
        priceDigits() {
            return this.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        },
    });
});

app.post('/total', checkMiddleware, (req, res) => {
    amount = JSON.stringify(req.body.amount);

    const data = {
        amount: amount,
        consumerToken: consumerAccessToken,
        info: 'Худалдан авсан барааны нэхэмжлэх',
    };
    const requestData = {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            authorization: `Bearer ${accessToken}`,
        },
        data: JSON.stringify(data),
        url: 'http://192.168.3.90:10022/consumer/invoice',
    };
    axios(requestData).then((response) => {
        terminalId = response.data.terminalId;
        id = response.data.id;
        info = response.data.info;
        res.send({
            amount,
            info,
            terminalId,
            id
        });
    }).catch((err) => {
        console.log(err)
    });
});

app.post('/webhook', (req, res) => {
    const hook = {
        amount: req.body.amount,
        info: req.body.info,
        invoiceId: req.body.invoiceId,
        invoiceState: req.body.invoiceState,
        phoneNumber: req.body.phoneNumber
    };
    //......
    res.send(hook);
});

app.listen(8081, () => console.log(`App listening on ${PORT} port!`));